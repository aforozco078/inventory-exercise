package com.Inventory.Inventory.repository;

import com.Inventory.Inventory.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, String> {
}
