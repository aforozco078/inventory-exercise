package com.Inventory.Inventory.repository;

import com.Inventory.Inventory.domain.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<Users, Long> {

    public Users findByUsername(String username);
}
