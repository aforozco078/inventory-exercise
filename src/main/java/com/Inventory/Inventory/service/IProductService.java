package com.Inventory.Inventory.service;

import com.Inventory.Inventory.domain.Product;
import com.Inventory.Inventory.service.dto.ProductDTO;

import java.util.List;

public interface IProductService {

    public List<ProductDTO> getAll();

    public ProductDTO save(ProductDTO productDTO);

}
