package com.Inventory.Inventory.service.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

@Setter
@Getter

public class ProductDTO implements Serializable {

    @Id
    private long reference;

    @Column(length = 20)
    private String name;
}
