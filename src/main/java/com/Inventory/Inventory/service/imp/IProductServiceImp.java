package com.Inventory.Inventory.service.imp;

import com.Inventory.Inventory.repository.ProductRepository;
import com.Inventory.Inventory.service.IProductService;
import com.Inventory.Inventory.service.dto.ProductDTO;
import com.Inventory.Inventory.service.dto.transformer.ProductTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class IProductServiceImp implements IProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<ProductDTO> getAll() {
        return productRepository.findAll().stream()
                .map(ProductTransformer::getProductDTOFromProduct)
                .collect(Collectors.toList());
    }

    @Override
    public ProductDTO save(ProductDTO productDTO) {
        /*
        Product product = ProductTransformer.getProductFromProductDTO(productDTO);
        Product result = productRepository.save(productDTO);
        return ProductTransformer.getProductDTOFromProduct(result);
        */

        return ProductTransformer.getProductDTOFromProduct(productRepository.save(ProductTransformer.getProductFromProductDTO(productDTO)));
    }
}
