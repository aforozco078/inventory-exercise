package com.Inventory.Inventory.web.rest;


import com.Inventory.Inventory.service.IProductService;
import com.Inventory.Inventory.service.dto.ProductDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping
@RestController("/api")
public class ProductResource {

    @Autowired
    IProductService iProductService;

    @PostMapping("/get-product")
    public List<ProductDTO> getAll() {
        return iProductService.getAll();
    }

    @PostMapping("/save-product")
    public ProductDTO save(@RequestBody ProductDTO productDTO) {
        return iProductService.save(productDTO);
    }
}
